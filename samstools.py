bl_info = {
    "name": "Sam's Tools",
    "author": "Sam Baas",
    "category": "Tools",
    "version": (0, 0, 4),
    "blender": (2, 79, 0),
    "location": "View3D > Tools > Sam's Tools",
    "description": "Some handy tools for easy FBX export, forcing 1,1,1 scale etc.",
}

import bpy
import bmesh
import os
import io_scene_fbx.export_fbx
from math import pi

defaultNameFilter = ['Cube', 'Sphere', 'Icosphere', 'Cylinder','Torus','Plane','Torus','Cone','Circle']

class FakeOp:
	def report(self, tp, msg):
		print("%s: %s" % (tp, msg))

def objectToLayer( object, layer ):
    layers = [False]*20
    layers[layer] = True
    object.layers = layers
  
# vector substration
def vecsub(a, b):
    return [a[0] - b[0], a[1] - b[1], a[2] - b[2]]

# vector crossproduct
def veccross(x, y):
    v = [0, 0, 0]
    v[0] = x[1]*y[2] - x[2]*y[1]
    v[1] = x[2]*y[0] - x[0]*y[2]
    v[2] = x[0]*y[1] - x[1]*y[0]
    return v

# calculate normal from 3 verts
def Normal(v0, v1, v2):
    return veccross(vecsub(v0, v1),vecsub(v0, v2)) 

def Normal4(v0, v1, v2, v3):
    return veccross(vecsub(v0, v2),vecsub(v1, v3))

class MaterialColorPanel(bpy.types.Panel):
    bl_label = "Sam's Tools vertex bake color"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "material"
    

    def color_get(self):
        print("X")
        return self["color_Material"]


    def color_update(self, context):
        col_R=self.color_Material[0]
        col_G=self.color_Material[1]
        col_B=self.color_Material[2]
        bpy.context.object.active_material.diffuse_color=(col_R,col_G,col_B)
        #print(">UPDATE:", col_R, col_G, col_B)
        
    
    bpy.types.Scene.color_Material=bpy.props.FloatVectorProperty(   
                                            name="Vertex bake color", 
                                            subtype = "COLOR", 
                                            default = [1.0,0.0,0.0],
                                            update=color_update                                   
                                            )            
    def execute(self,context):
        return{"FINISHED"}

    def draw(self, context): 
        self.layout.prop(context.scene,"color_Material")
  
class SamsTools(bpy.types.Panel):
    bl_label = "Sam's Tools"
    bl_idname = "OBJECT_PT_samstools"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_category = "Sam's Tools"
    
    def draw(self, context):
        layout = self.layout

        row = layout.row()
        row.operator("button.findobjectswithoutuv",icon='TEXTURE_DATA')
        row = layout.row()
        row.operator("button.finddefaultnames",icon='MESH_CUBE')
        row = layout.row()
        row.operator("button.fixrotationforunity",icon='EMPTY_DATA')
        row = layout.row()
        row.operator("button.clearalltexturereferences",icon='IMAGE_DATA')
        row = layout.row()
        row.operator("button.materialtovertexcolor",icon='GROUP_VCOL')
        row = layout.row()
        row.operator("button.clearmaterials",icon='MATERIAL_DATA')
        layout.row().separator()
        row = layout.row()
        row.operator("button.localcoordinatestonormal",icon='EMPTY_DATA')   
        row = layout.row()
        row.operator("button.addcubeonnormal",icon='MESH_CUBE')   
        row = layout.row()
        row.operator("button.addcylinderonnormal",icon='MESH_CYLINDER')   
        row = layout.row()
        row.operator("button.origintoselection",icon='EMPTY_DATA')  
        layout.row().separator()
        row = layout.row()
        row.operator("button.applyallscales",icon='RESTRICT_SELECT_OFF')   
        row = layout.row()
        row.operator("button.moveselectedtozero",icon='OBJECT_DATA')   
        layout.row().separator()
        row = layout.row()
        row.operator("button.generatelods",icon='MESH_ICOSPHERE')   
        layout.row().separator()
        row = layout.row()
        row.prop(context.scene,"fbx_foldername")
        row = layout.row()
        row.operator("button.directfbx",icon='FILE')

class GenerateLODS(bpy.types.Operator):
    bl_idname = "button.generatelods"
    bl_label = "Generate LODS"

    #Clear all texture slots
    def execute(self, context):
        #Put object on 3 layers with decimate modifier
        #Selected on 0
        lod0 = bpy.context.scene.objects.active
        objectToLayer(lod0,0)
        basename = lod0.name.replace('_LOD0','')
        lod0.name = basename+'_LOD0'
        
        #LOD1
        lod1 = lod0.copy()
        lod1.animation_data_clear()
        bpy.context.scene.objects.link(lod1)
        mod_decimate1 = lod1.modifiers.new('Decimate', 'DECIMATE')
        mod_decimate1.ratio = 0.5  
        lod1.name = basename+'_LOD1'
        objectToLayer(lod1,1)
        #LOD2
        lod2 = lod0.copy()
        lod2.animation_data_clear()
        bpy.context.scene.objects.link(lod2)
        mod_decimate2 = lod2.modifiers.new('Decimate', 'DECIMATE')
        mod_decimate2.ratio = 0.1  
        lod2.name = basename+'_LOD2'
        objectToLayer(lod2,2)
    
        return {'FINISHED'} 
        
class FindDefaultNames(bpy.types.Operator):
    bl_idname = "button.finddefaultnames"
    bl_label = "Select default named objects"

    #Clear all texture slots
    def execute(self, context):
        #Find all objects that still have default'ish names like Cube.002 etc
        for obj in bpy.data.objects:
            if any(ext in obj.name for ext in defaultNameFilter):
                obj.select = True
            else:
                obj.select = False
        return {'FINISHED'}         

class BakeMaterialToVertexColor(bpy.types.Operator):
    bl_idname = "button.materialtovertexcolor"
    bl_label = "Bake objects materials to vertex color"

    def execute(self, context):
        #Apply material color to vertex color
        for obj in bpy.context.selected_objects:
            obj.select = False
            if obj.type == 'MESH':
                me = obj.data
                if me.vertex_colors.active is None:
                    me.vertex_colors.new()

                for poly in me.polygons:
                    slot = obj.material_slots[poly.material_index]
                    mat = slot.material
                    for loop in poly.loop_indices:
                        me.vertex_colors.active.data[loop].color = mat.diffuse_color
                me.update()
        return {'FINISHED'}     
        
class FindUVless(bpy.types.Operator):
    bl_idname = "button.findobjectswithoutuv"
    bl_label = "Select objects without UV's"

    #Clear all texture slots
    def execute(self, context):
        #Find all objects that still have default'ish names like Cube.002 etc
        for obj in bpy.data.objects:
            obj.select = False
            if obj.type == 'MESH':
                if obj.data.uv_layers:
                    obj.select = False
                else:
                    obj.select = True
                

        return {'FINISHED'}            
        
class ClearAllTextureReferences(bpy.types.Operator):
    bl_idname = "button.clearalltexturereferences"
    bl_label = "Clear all UV texture slots"

    #Clear all texture slots
    def execute(self, context):
        #Unlink all images. This stuff ruins our material slots in Unity.
        nr = 0
        for img in bpy.data.images:
            img.user_clear()
            nr = nr+1
        self.report({'INFO'}, "Cleared " + str(nr) + " images")
        return {'FINISHED'}
        
class FixRotationForUnity(bpy.types.Operator):
    bl_idname = "button.fixrotationforunity"
    bl_label = "Fix (0,0,0) rotation for Unity"

    #Somehow only works for one object now..
    def execute(self, context):
        for obj in bpy.context.selected_objects:
            #if multiuser, make single user first
            self.report({'INFO'}, "Fixed rotation for " + obj.name)
            bpy.context.scene.objects.active = obj
            #Rotate object over x b 90 degrees. Apply the rotation, and rotate the object back to its original visual rotation
            obj.rotation_euler = (obj.rotation_euler[0]+(pi * -90 / 180),obj.rotation_euler[1],obj.rotation_euler[2])
            bpy.ops.object.transform_apply(location = False, scale = False, rotation = True)
            obj.rotation_euler = (obj.rotation_euler[0]+(pi * 90 / 180),obj.rotation_euler[1],obj.rotation_euler[2]) 
        return {'FINISHED'}
        
class ApplyAllScales(bpy.types.Operator):
    bl_idname = "button.applyallscales"
    bl_label = "Apply (1,1,1) scales"

    def execute(self, context):
        for obj in bpy.data.objects:
            if (obj.scale[0] != 1) or (obj.scale[1] != 1) or (obj.scale[2] != 1):
                print("Applying transform for " + obj.name)
                obj.select = True
            else:
                obj.select = False
            bpy.ops.object.transform_apply(location = False, scale = True, rotation = False)
            self.report({'INFO'}, "Applied (1,1,1) scales")
        return {'FINISHED'}
        
class MoveSelectedToZero(bpy.types.Operator):
    bl_idname = "button.moveselectedtozero"
    bl_label = "Move selected objects to 0,0,0"

    def execute(self, context):
        for obj in bpy.context.selected_objects:
            obj.location = [0,0,0]
        return {'FINISHED'}
        
        
class AddCubeOnNormal(bpy.types.Operator):
    bl_idname = "button.addcubeonnormal"
    bl_label = "Add cube on normal"

    def execute(self, context):
        obj = bpy.context.edit_object
        myname = obj.name
        objdata = obj.data
        bm = bmesh.from_edit_mesh(objdata)
        
        wmtx = obj.matrix_world
        
        bpy.ops.mesh.select_mode(type="FACE")
        
        #store current viewport
        viewmatrix = bpy.context.space_data.region_3d.view_matrix.copy()
        
        for f in bm.faces:
            if f.select:
                bpy.ops.view3d.snap_cursor_to_selected()
                
                f.select = False
                
                bpy.ops.view3d.viewnumpad(type='TOP', align_active=True)
                
                v3d = bpy.context.space_data
                v3d.transform_orientation = 'VIEW'
                bpy.ops.transform.transform(mode = 'ALIGN')
                
                bpy.ops.object.mode_set(mode='OBJECT')

                bpy.ops.mesh.primitive_cube_add(location=bpy.context.scene.cursor_location,view_align = True)
                    
                v3d.transform_orientation = 'GLOBAL'
                bpy.context.space_data.region_3d.view_matrix = viewmatrix
                
                return {'FINISHED'}

class AddCylinderOnNormal(bpy.types.Operator):
    bl_idname = "button.addcylinderonnormal"
    bl_label = "Add cylinder on normal"

    def execute(self, context):
        obj = bpy.context.edit_object
        myname = obj.name
        objdata = obj.data
        bm = bmesh.from_edit_mesh(objdata)
        
        wmtx = obj.matrix_world
        
        bpy.ops.mesh.select_mode(type="FACE")
        
        #store current viewport
        viewmatrix = bpy.context.space_data.region_3d.view_matrix.copy()
        
        for f in bm.faces:
            if f.select:
                bpy.ops.view3d.snap_cursor_to_selected()
                
                f.select = False
                
                bpy.ops.view3d.viewnumpad(type='TOP', align_active=True)
                
                v3d = bpy.context.space_data
                v3d.transform_orientation = 'VIEW'
                bpy.ops.transform.transform(mode = 'ALIGN')
                
                bpy.ops.object.mode_set(mode='OBJECT')

                bpy.ops.mesh.primitive_cylinder_add(location=bpy.context.scene.cursor_location,view_align = True)
                    
                v3d.transform_orientation = 'GLOBAL'
                bpy.context.space_data.region_3d.view_matrix = viewmatrix
                
                return {'FINISHED'}
        
        
class LocalCoordinatesToNormal(bpy.types.Operator):
    bl_idname = "button.localcoordinatestonormal"
    bl_label = "Local coordinates to normal"

    def execute(self, context):
        obj = bpy.context.edit_object
        myname = obj.name
        objdata = obj.data
        bm = bmesh.from_edit_mesh(objdata)
        
        wmtx = obj.matrix_world
        
        previousParent = obj.parent
        previousChildren = obj.children
        
        bpy.ops.mesh.select_mode(type="FACE")
        
        #store current viewport
        v3d = bpy.context.space_data
        viewmatrix = v3d.region_3d.view_matrix.copy()
        
        for f in bm.faces:
            if f.select: 
                bpy.ops.view3d.snap_cursor_to_selected()
                
                f.select = False
                
                bpy.ops.view3d.viewnumpad(type='TOP', align_active=True)
                
                v3d.transform_orientation = 'VIEW'
                bpy.ops.transform.transform(mode = 'ALIGN')
                
                bpy.ops.object.mode_set(mode='OBJECT')
                
                bpy.ops.object.select_all(action='DESELECT')

                bpy.ops.mesh.primitive_plane_add(location=bpy.context.scene.cursor_location,view_align = True)
                newplane = bpy.context.scene.objects.active
                
                bpy.ops.object.mode_set(mode='OBJECT')
                for v in bpy.context.object.data.vertices:
                    v.select = True
                bpy.ops.object.mode_set(mode='EDIT')
                bpy.ops.mesh.delete(type='VERT')
                bpy.ops.object.mode_set(mode='OBJECT')
                
                newplane.select = False
                obj.select = True
                newplane.select = True
                bpy.ops.object.join()
                bpy.context.scene.objects.active.name = myname

                #set children parent to dummy plane
                for childobject in previousChildren:
                    childobject.select = True
                    bpy.context.scene.objects.active = newplane
                    bpy.ops.object.parent_set()
                
                #parent out new object to the parent of our old object
                if previousParent is not None:
                    newplane.select = True
                    previousParent.select = True
                    bpy.context.scene.objects.active = previousParent
                    bpy.ops.object.parent_set()
                    previousParent.select = False
                
                v3d.transform_orientation = 'GLOBAL'  
                bpy.ops.view3d.viewnumpad(type='FRONT')

                #back into editing our object
                bpy.ops.object.select_all(action='DESELECT')
                newplane.select = True
                bpy.context.scene.objects.active = newplane
                bpy.ops.object.mode_set(mode='EDIT')
                
                v3d.region_3d.view_matrix = viewmatrix
                
                return {'FINISHED'}
		
class SetOriginToSelection(bpy.types.Operator):
    bl_idname = "button.origintoselection"
    bl_label = "Set origin to selection"

    def execute(self, context):
        obj = bpy.context.edit_object
        myname = obj.name
        objdata = obj.data
        bm = bmesh.from_edit_mesh(objdata)
        
        wmtx = obj.matrix_world
        
        previousParent = obj.parent
        previousChildren = obj.children
        
        bpy.ops.view3d.snap_cursor_to_selected()
        bpy.ops.object.mode_set(mode='OBJECT')

        bpy.ops.object.select_all(action='DESELECT')

        bpy.ops.mesh.primitive_plane_add(location=bpy.context.scene.cursor_location)
        newplane = bpy.context.scene.objects.active
        
        bpy.ops.object.mode_set(mode='OBJECT')
        for v in bpy.context.object.data.vertices:
            v.select = True
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.mesh.delete(type='VERT')
        bpy.ops.object.mode_set(mode='OBJECT')
        
        newplane.select = False
        obj.select = True
        newplane.select = True
        bpy.ops.object.join()
        bpy.context.scene.objects.active.name = myname
        
        #set children parent to dummy plane
        for childobject in previousChildren:
            childobject.select = True
            bpy.context.scene.objects.active = newplane
            bpy.ops.object.parent_set()
        #parent out new object to the parent of our old object
        if previousParent is not None:
            newplane.select = True
            previousParent.select = True
            bpy.context.scene.objects.active = previousParent
            bpy.ops.object.parent_set()
            previousParent.select = False
        
        #back into editing our object
        bpy.ops.object.select_all(action='DESELECT')
        newplane.select = True
        bpy.context.scene.objects.active = newplane
        bpy.ops.object.mode_set(mode='EDIT')
        
        return {'FINISHED'}        


class ButtonDirectFbx(bpy.types.Operator):
    bl_idname = "button.directfbx"
    bl_label = "Directly export to FBX"

    def execute(self, context):
        kwargs = io_scene_fbx.export_fbx.defaults_unity3d()
        
        #get absolute path to your folder (can be folder in hierarchy)
        abspath = bpy.path.abspath(context.scene.fbx_foldername)
                
        #get our .blend path and replace blend with fbx
        filenamefbx = bpy.path.basename(bpy.context.blend_data      .filepath).replace(".blend",".fbx")
        io_scene_fbx.export_fbx.save(FakeOp(), bpy.context, filepath=abspath+filenamefbx, **kwargs)
        self.report({'INFO'}, "Exported " + abspath+filenamefbx)
        return {'FINISHED'}

class ClearMaterials(bpy.types.Operator):
    bl_idname = "button.clearmaterials"
    bl_label = "Clear materials in selected objects"

    def execute(self, context):        
        #clear materials
        for obj in bpy.context.selected_objects:
            obj.select = False
            if obj.type == 'MESH':
                obj.data.materials.clear()
                
        self.report({'INFO'}, "Cleared materials")
        
        return {'FINISHED'}
        
def register():
    bpy.utils.register_module(__name__)
    bpy.types.Scene.fbx_foldername = bpy.props.StringProperty(name="FBX relative target folder",description="The relative path to the FBX file",default="//",subtype = 'DIR_PATH')

def unregister():
    bpy.utils.unregister_module(__name__)
    del bpy.types.Scene.fbx_foldername


if __name__ == "__main__":
    register()